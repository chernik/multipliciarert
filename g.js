import {n2m} from './nm.js';

function sm(m){
    let res = 0;
    for(let a of m){
        res += a;
    }
    return res;
}

export function g(n){
    const res = {};
    let lastS = null;
    let cnt = 0;
    while(n !== 1){
        const s = sm(n2m(n));
        n = (n % 2) === 0 ? n / 2 : 3 * n + 1;
        if(lastS === null){
            lastS = s;
            continue;
        }
        if(s >= lastS){
            if(res[lastS] === undefined){
                cnt++;
                res[lastS] = new Set();
            }
            res[lastS].add(s);
        }
        lastS = s;
    }
    if(lastS !== null){
        if(res[lastS] === undefined){
            cnt++;
            res[lastS] = new Set();
        }
        res[lastS].add(0);
    }
    for(let k in res){
        const arr = Array.from(res[k]);
        arr.sort((a, b) => a - b);
        res[k] = arr;
    }
    return [res, cnt];
}

export function reduceG(g){
    let min = null;
    for(let k in g){
        if(min === null || min > +k){
            min = +k;
        }
    }
    if(min !== 1){
        console.log(g);
        throw 'Nobegin graph';
    }
    let a = min;
    const res = {};
    const metric = {
        first: 0,
        last: 0,
        cnt: 0,
    };
    for(; g[a] !== undefined; a++){
        const reduced = g[a].filter(b => b >= a);
        if(reduced.length === 0){
            continue;
        }
        if(metric.first === null){
            metric.first = a;
        }
        metric.last = a;
        metric.cnt++;
        res[a] = reduced;
    }
    for(let k in g){
        if(+k < 1 || +k > a){
            console.log(g);
            throw 'Splitted graph';
        }
    }
    return [res, metric];
}
