export function encode(d){
    let r = 1;
    let isBig = false;
    for(let n of d){
        let pw = Math.pow(16, n[0] ?? n);
        pw = n[0] === undefined ? pw : pw * 4;
        let rn = r * (isBig ? BigInt(pw) : pw);
        if(!isBig && rn > Number.MAX_SAFE_INTEGER){
            rn = BigInt(r) * BigInt(pw);
            isBig = true;
        }
        let k = 1;
        const rest = Number(rn % (isBig ? 9n : 9));
        switch(rest){
            case 1:
                k = 4; break;
            case 2:
            case 8:
                k = 2; break;
            case 5:
                k = 8; break;
            default:
                break;
        }
        if(pw === 1 && k === 1){
            k = rest === 7 ? 16 : 4;
        }
        r = rn * (isBig ? BigInt(k) : k);
        if(!isBig && r > Number.MAX_SAFE_INTEGER){
            r = BigInt(rn) * BigInt(k);
            isBig = true;
        }
        r = isBig ? (r - 1n) / 3n : (r - 1) / 3;
        if(isBig && r <= Number.MAX_SAFE_INTEGER){
            r = Number(r);
            isBig = false;
        }
        if((r % (isBig ? 3n : 3)) == 0){
            throw "Dead";
        }
    }
    if(r <= Number.MAX_SAFE_INTEGER){
        r = Number(r);
    }
    return r;
}

export function getPath(r){
    let isBig = r > Number.MAX_SAFE_INTEGER;
    r = isBig ? BigInt(r) : Number(r);
    const d = [];
    if((r % (isBig ? 2n : 2)) == 0){
        return null;
    }
    while(isBig || r !== 1){
        let rn = r * (isBig ? 3n : 3);
        if(!isBig && rn > Number.MAX_SAFE_INTEGER){
            r = BigInt(r) * 3n + 1n;
            isBig = true;
        } else {
            r = rn + (isBig ? 1n : 1);
        }
        let k = 0;
        while((r % (isBig ? 2n : 2)) == 0){
            r /= isBig ? 2n : 2;
            k++;
        }
        if(isBig && rn <= Number.MAX_SAFE_INTEGER){
            r = Number(r);
            isBig = false;
        }
        k = (k % 2) === 0 ? k / 2 : (k - 1) / 2;
        d.push(k);
    }
    if(d.length === 0){
        return d;
    }
    d.reverse();
    // d[0]--;
    const res = d.map(a => (a % 2) === 0 ? a / 2 : [(a - 1) / 2]);
    res.toString = () => `[${
        res.map(a => a[0] === undefined ? ` ${a}` : `!${a[0]}`).join(', ')
    }]`;
    return res;
}

export function binEncode(n){
    const p = [];
    while(n !== 0){
        p.push(n % 2);
        n = (n % 2) === 0 ? n / 2 : (n - 1) / 2;
    }
    return encode(p);
}

function c(ch){
    return ch.charCodeAt(0);
}

export const strCoder = {
    encode(s){
        const d = s.split('').map(a => {
            if(a >= 'a' && a <= 'z'){
                return a.charCodeAt(0) - c('a') + 1;
            }
            if(a === ' '){
                return c('z') - c('a') + 2;
            }
            return c('z') - c('a') + 3;
        });
        return encode(d);
    },
    decode(n){
        const d = getPath(n);
        return d.map(a => {
            a = a[0] ?? a;
            a -= 1;
            if(a > c('z') - c('a')){
                a -= c('z') - c('a');
                switch(a){
                    case 1: return ' ';
                    default: return '?';
                }
            }
            return String.fromCharCode(a + c('a'));
        }).join('');
    }
}
