import {test, ensureM, n2m, m2n, m2s} from './nm.js';
import {g, reduceG} from './g.js';
import {binEncode, encode, getPath} from './enc.js';

function is2(n){
    while((n % 2) === 0){
        n /= 2;
    }
    return n === 1;
}

const div1 = document.createElement('pre');
const div2 = document.createElement('pre');
document.body.appendChild(div1);
document.body.appendChild(div2);

async function match1(){
    let max = 1;
    const mmm = 1;
    for(let i = 2; i < mmm; i++){
        if((i % 10) === 0){
            div1.innerText = `From ${mmm} matched ${i}`;
            await new Promise(cb => setTimeout(cb, 0));
        }
        const [g1, cnt] = g(i);
        // const [g2, metric] = reduceG(g1);
        // const cnt = metric.cnt;
        if(cnt === 0){
            if(is2(i)){
                continue;
            }
            console.log(i, g2);
            throw 'wow';
        }
        if(cnt === 1){
            continue;
        }
        if(cnt <= max){
            continue;
        }
        max = cnt;
        div2.innerText += `${i}: ${cnt} ${JSON.stringify(g1, null, 4)}\n\n`;
    }
    div1.innerText = `ALL ${mmm}`;
}

async function match2(){
    const d = {};
    let lastA = 2;
    for(let aa = 1; aa < 15; aa++){
        console.log(lastA, d);
        for(let a = lastA; a < 100000; a++){
            if((a % 100) === 0){
                console.log('check', a);
            }
            const m = [[1, 0, aa], [a, 0, 1]];
            let b = m2n(ensureM(m));
            const c = b;
            let it = 1;
            while(true){
                while((b % 2) === 0){
                    b /= 2;
                }
                b = b * 3 + 1;
                it++;
                if((it % 1000) === 0){
                    console.log(`${it} itrs`);
                }
                if(b > c){
                    break;
                }
                if(b === 4){
                    break;
                }
            }
            if(b === 4){
                continue;
            }
            d[aa] = [a, c];
            lastA = a;
            break;
        }
    }
    console.log('ALL', d);
}

function match3(){
    const res = {};
    const normals = [];
    const fixedd = [];
    let cntAll = 0;
    for(let i = 5; i < 10000; i += 2){
        if((i % 3) === 0){
            continue;
        }
        cntAll++;
        const p = getPath(i);
        const n = encode(p);
        if(i !== n){
            console.log('zero moves', [i, n], p.toString());
            throw "Zero moves";
        }
        let hasSamePath = false;
        let hasFixedHalfs = false;
        for(let j = 0; j < p.length; j++){
            // Try to increase
            let oldV = p[j];
            p[j] = oldV[0] === undefined ? [oldV - 1] : oldV[0];
            const nMoved = encode(p);
            if(nMoved === n){
                hasSamePath = true;
                if(oldV[0] !== undefined){
                    oldV = oldV[0];
                }
            } else if(oldV[0] !== undefined){
                hasFixedHalfs = true;
            }
            p[j] = oldV;
        }
        if(!hasSamePath){
            // console.log(n, 'unique');
        }
        const k = p.toString();
        if(!hasFixedHalfs){
            normals.push([k, n]);
        } else {
            fixedd.push([k, n]);
        }
        res[k] = res[k] ?? [];
        if(res[k].length === 1){
            console.log('Repeat for ', res[k]);
        }
        console.assert(encode(p) === i);
        res[k].push(i);
    }
    const arr = [];
    for(let k in res){
        arr.push([k, res[k]]);
    }
    arr.sort((a, b) => a[0].length - b[0].length);
    normals.sort((a, b) => a[0].length - b[0].length);
    // fixedd.sort((a, b) => a[0].length - b[0].length);
    for(let i = 0; i < arr.length; i++){
        if(arr[i][0].length < 15){ // 15 is mas string length of path
            continue;
        }
        for(let j = 0; j < arr.length; j++){
            if(i === j || arr[j].length === 3){
                continue;
            }
            if(arr[j][0].indexOf(arr[i][0].slice(0, -1)) === 0){
                arr[j].push(false);
            }
        }
    }
    arr.slice(0, 100).forEach(a => {
        if(a.length >= 2){
            console.log(a[0], a[1]);
        }
    });
    fixedd.forEach(a => {
        // console.log(a[0], a[1]);
    });
    console.log(normals.length, fixedd.length, cntAll, normals.length + fixedd.length);
}

function match(){
    const res = new Set();
    for(let i = 0; i < 1000000; i++){
        const n = binEncode(i);
        if(n < 1000){
            res.add(n);
        }
    }
    const arr = Array.from(res);
    arr.sort((a, b) => a - b);
    console.log(arr.join('\n'));
}

match3();

// test();
/*
function sm(m){
    let res = 0;
    for(let a of m){
        res += a;
    }
    return res;
}

const max = {
    sm: null,
    m2s: null,
};

function findMax(cur, id){
    for(let k in cur){
        if(max[k] === null || max[k][0] < cur[k]){
            max[k] = [cur[k], id];
        }
    }
}

for(let i = 1; i < 10000; i++){
    const a = n2m(i);
    const as = {
        sm: sm(a),
        m2s: m2s(a).length,
    };
    findMax(as, i);
}

console.log(max);
*/

