let simples = [-1, -1];
for(let i = 2; i < 1000000; i++){
    if(simples[i] === -1){
        continue;
    }
    for(let j = i; j < 1000000; j += i){
        simples[j] = -1;
    }
    simples[i] = i;
}
simples = simples.filter(a => a !== -1);
console.log(simples);

export function n2m(n){
    const res = [];
    for(let a = 1; ; a++){
        const simple = simples[a-1];
        let lres = 0;
        while((n % simple) === 0){
            n /= simple;
            lres++;
        }
        if(lres !== 0){
            res.push([a, simple, lres]);
        }
        if(n === 1){
            break;
        }
    }
    if(n !== 1){
        throw "Need more simples";
    }
    return res;
}

export function ensureM(m){
    m.forEach(a => {
        if(a[1] !== 0){
            return;
        }
        let na = simples[a[0] - 1];
        if(na === undefined){
            throw "Error";
        }
        a[1] = na;
    });
    return m;
}

export function m2n(m){
    let res = 1;
    m.forEach(a => {
        for(let w = 0; w < a[2]; w++){
            res *= a[1];
        }
    });
    return res;
}

export function m2s(m){
    return m
        .map(a => `${a[1]}[${a[0]}]^${a[2]}`)
        .join(' * ');
}

export function test(){
    for(let i = 1; i < 100; i++){
        if(m2n(n2m(i)) !== i){
            debugger;
            m2n(n2m(i));
            throw 'Wrong tests';
        }
    }
}

export function rotates(n, f){
    if(typeof(f) === 'number'){
        const fv = f;
        f = v => v * fv;
    }
    const res = [];
    const was = new Set();
    for(let i = 0; i < n; i++){
        if(was.has(i)){
            continue;
        }
        const loop = [];
        const loopWas = new Set();
        let m = i;
        while(!loopWas.has(m)){
            loop.push(m);
            loopWas.add(m);
            if(was.has(m)){
                loop.push('!');
                break;
            }
            m = f(m) % n;
        }
        for(m of loopWas){
            was.add(m);
        }
        res.push(loop.join('-'));
    }
    return res.join(' ');
}
